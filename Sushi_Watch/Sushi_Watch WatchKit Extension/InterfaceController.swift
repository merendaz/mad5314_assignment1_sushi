//
//  InterfaceController.swift
//  Sushi_Watch WatchKit Extension
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-11-03.
//  Copyright © 2019 Medtouch. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    @IBOutlet weak var gameStatusLbl: WKInterfaceLabel!
    
    @IBOutlet weak var startBtnOutlet: WKInterfaceButton!
    @IBOutlet weak var stopBtnOutlet: WKInterfaceButton!
    
    @IBOutlet weak var powerUpBtnOutlet: WKInterfaceButton!
    var isPlaying   = false
    var reset       = true
    var seconds     = 25
    var powerUps    = 2
    var powerUp     = 0
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    // MARK: Receive messages from Phone
    // -----------------------------------
    // 3. This function is called when Watch receives message from Phone
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
//        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        if (message["time"] != nil) {
            let time = message["time"] as! String
//            var curTime = time
//            self.seconds = Int(time)!-1
//            self.timeLbl.setText("Time:\(time) s")
            // Time remaining warnings!
            if (time == "15") {
                self.powerUpBtnOutlet.setHidden(true)
                self.gameStatusLbl.setText("\(time) s remaining...")
            }
            if (time == "10") {
                self.gameStatusLbl.setText("\(time) s remaining...")
            }
            if (time == "5") {
                self.gameStatusLbl.setText("\(time) s remaining...")
            }
            if (time == "0") {
                self.gameStatusLbl.setHidden(false)
                self.gameStatusLbl.setText("  GAME OVER!!")
                self.startBtnOutlet.setBackgroundImageNamed("playBtn60_30")
                self.isPlaying = false
                self.reset = true
            }
            
            
            let randPowerUp1 = Int.random(in: 10...13)
            let randPowerUp2 = Int.random(in: 3...9)
            if (self.powerUps > 0) {
                if ((Int(time)! < randPowerUp1) && (Int(time)! > (randPowerUp1 - 2))) {
                    self.gameStatusLbl.setText("")
                    self.powerUpBtnOutlet.setHidden(false)
                    self.powerUps -= 1
                }
                else if (Int(time)! < (randPowerUp1 - 2)) {
                    self.powerUpBtnOutlet.setHidden(true)
                }
                
                if ((Int(time)! < randPowerUp2) && (Int(time)! > (randPowerUp2 - 2))) {
                    self.gameStatusLbl.setText("")
                    self.powerUpBtnOutlet.setHidden(false)
                    self.powerUps -= 1
                }
                else if (Int(time)! < (randPowerUp2 - 2)) {
                    self.powerUpBtnOutlet.setHidden(true)
                }
            }
         }
    }
    
    func randPowerUp() -> Int {
        return Int.random(in: 0...12)
    }

    func sendToPhone(message:[String:String]){
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
//            print("\nMessage sent to phone")
        }
        else {
            print("WATCH: Cannot reach phone")
        }
    }
    
    @IBAction func tapLeftBtnPressed() {
        self.sendToPhone(message: ["direction":"left"])
    }
    @IBAction func tapRightBtnPressed() {
        self.sendToPhone(message: ["direction":"right"])
    }
    @IBAction func startBtnPressed() {
        stopBtnOutlet.setEnabled(true)
        if ((!self.isPlaying) && (self.reset)) {
            print("Play pressed!")
            self.sendToPhone(message: ["start":"true", "reset":"true"])
            self.isPlaying = true
            self.reset = false
            startBtnOutlet.sizeToFitWidth()
            startBtnOutlet.setBackgroundImageNamed("pauseBtn60_30")
        }
        else if ((self.isPlaying) && (!self.reset)) {
            self.sendToPhone(message: ["start":"false", "reset":"false"])
            self.isPlaying = false
            startBtnOutlet.sizeToFitWidth()
            startBtnOutlet.setBackgroundImageNamed("playBtn60_30")
        }
        else if ((!self.isPlaying) && (!self.reset)) {
            self.sendToPhone(message: ["start":"false", "reset":"false"])
            self.isPlaying = true
            startBtnOutlet.sizeToFitWidth()
            startBtnOutlet.setBackgroundImageNamed("pauseBtn60_30")
        }
    }
    
    @IBAction func stopBtnPressed() {
        self.isPlaying  = false
        self.reset      = true
        self.seconds    = 25
        self.powerUps   = 2
        startBtnOutlet.setBackgroundImageNamed("playBtn60_30")
        self.sendToPhone(message: ["start":"false", "reset":"true"])
    }
    
    @IBAction func powerUpBtnPressed() {
        self.sendToPhone(message: ["powerup" : "true"])
    }
}
