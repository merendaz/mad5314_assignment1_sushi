//
//  ViewController.swift
//  Sushi_Watch
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-11-03.
//  Copyright © 2019 Medtouch. All rights reserved.
//
import UIKit
import SpriteKit
import GameplayKit
import WatchConnectivity

class ViewController : UIViewController
{
    var scene : GameScene! = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.scene = GameScene(size:self.view.bounds.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill
        
        // property to show hitboxes
        skView.showsPhysics = true
        
        skView.presentScene(scene)
    }
}
