//
//  GameScene.swift
//  Sushi_Watch
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-11-03.
//  Copyright © 2019 Medtouch. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity

class GameScene: SKScene, WCSessionDelegate {
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    // Make a tower
    var sushiTower:[SKSpriteNode] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    
    // Make chopsticks
    var chopstickGraphicsArray:[SKSpriteNode] = []
    
    // Make variables to store current position
    var catPosition = "left"
    var chopstickPositions:[String] = []
    
    var numLoops    = 0
    var isRunning   = false
    var seconds     = 25
    var numPoints   = 0
    var isPowerUP   = false
    var powerUps    = 2
    
    
    var timeLabel   : SKLabelNode!
    var pointsLabel : SKLabelNode!
    
    
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Watch")
        // Message from phone comes in this format: ["course":"MADT"]
        if ((message["direction"] != nil) && (isRunning)) {
            let direction = message["direction"] as! String
        
            if (direction == "left") {
                self.moveCatToLeft()
            }
            else if (direction == "right") {
                self.moveCatToRight()
            }
        }
        if ((message["start"] != nil) && (message["reset"] != nil)) {
            let start = message["start"] as! String
            
            let reset =  message["reset"] as! String
            if ((start == "true") && (reset == "true"))  {
                self.gameReset()
                self.isRunning = true
            }
            else if ((start == "true") && (reset == "false"))  {
                self.isRunning = true
            }
            else if ((start == "false") && (reset == "false"))  {
                self.gameReset()
                self.isRunning = false
            }
            else if ((start == "false") && (reset == "true"))  {
                self.isRunning = false
            }
        }
        if (message["powerup"] != nil) {
            let powerup = message["powerup"] as! String
            if (powerup == "true") {
                self.isPowerUP = true
            }
//            self.seconds = Int(time)!
        }
    }
    
    func sendToWatch(message:[String:String]){
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            print("\nMessage sent to watch")
        }
        else {
            print("WATCH: Cannot reach watch")
        }
    }
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SKSpriteNode(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
        
        
        // -----------------------
        // MARK: PART 2: ADD CHOPSTICKS TO SUSHI
        // -----------------------
        
        // Generate  a random number (0, 1, 2)
        // 0 = no stick
        // ???????
        // 1 = stick on right
        // stick.position.x = sushi.position.x + 100
        // stick.position.y = sushi.position.y - 10
        // 2 = stick on left
        // stick.position.x = sushi.position.x - 100
        // stick.position.y = sushi.position.y - 10
        
        
        // generate a number between 1 and 2
        let stickPosition = Int.random(in: 1...2)
        print("Random number: \(stickPosition)")
        if (stickPosition == 1) {
            // save the current position of the chopstick
            self.chopstickPositions.append("right")
            
            // draw the chopstick on the screen
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x + 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick object to the array
            self.chopstickGraphicsArray.append(stick)
            
            // redraw stick facing other direciton
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            stick.run(facingRight)
        }
        else if (stickPosition == 2) {
            // save the current position of the chopstick
            self.chopstickPositions.append("left")
            
            // left
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x - 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick to the array
            self.chopstickGraphicsArray.append(stick)
        }
    }
    
    override func didMove(to view: SKView) {
        
        self.timeLabel = SKLabelNode(text: "Time: \(self.seconds) s")
        self.timeLabel.position = CGPoint(x:60, y:760)
        self.timeLabel.fontColor = UIColor.yellow
        self.timeLabel.fontSize = 24
        self.timeLabel.zPosition = 999
        self.timeLabel.fontName = "Arial Black"
        addChild(self.timeLabel)
        
        self.pointsLabel = SKLabelNode(text: "\(self.numPoints) points")
        self.pointsLabel.position = CGPoint(x:50, y:690)
        self.pointsLabel.fontColor = UIColor.green
        self.pointsLabel.fontSize = 24
        self.pointsLabel.zPosition = 999
        self.pointsLabel.fontName = "Arial Black"
        addChild(self.pointsLabel)

        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        else {
            print("Phone does not support WCSession")
        }
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
    }
    
    func buildTower() {
        for _ in 0...8 {
            self.spawnSushi()
        }
        for i in 0...8 {
            print(self.chopstickPositions[i])
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        if (self.isRunning) {
            if (self.seconds <= 0) {
                self.isRunning = false
            }
            self.numLoops += 1
            if self.sushiTower.count < 10
            {
                for _ in self.sushiTower.count...10
                {
                    self.spawnSushi()
                }
            }
            if (numLoops % 60 == 0) {
                if ((self.isPowerUP) && (self.powerUps > 0)) {
                    self.seconds += 10
                    self.isPowerUP = false
                    self.powerUps -= 1
                }
                self.seconds -= 1
                self.timeLabel.removeFromParent()
                self.pointsLabel.removeFromParent()
                // Add time label
                self.timeLabel = SKLabelNode(text: "Time:\n\(self.seconds) s")
                self.timeLabel.position = CGPoint(x:60, y:760)
                self.timeLabel.fontColor = UIColor.yellow
                self.timeLabel.fontSize = 24
                self.timeLabel.zPosition = 999
                self.timeLabel.fontName = "Arial Black"
                addChild(self.timeLabel)
                self.pointsLabel = SKLabelNode(text: "\(self.numPoints) points")
                self.pointsLabel.position = CGPoint(x:50, y:690)
                self.pointsLabel.fontColor = UIColor.green
                self.pointsLabel.fontSize = 24
                self.pointsLabel.zPosition = 999
                self.pointsLabel.fontName = "Arial Black"
                addChild(self.pointsLabel)
                
                self.sendToWatch(message: ["time" : "\(self.seconds)"])
                self.sendToWatch(message: ["points" : "\(self.numPoints)"])
            }
        }
    }
    
    func gameReset() {
        //Here goes the Initial data for a game
        self.seconds    = 25
        self.numLoops   = 0
        self.numPoints  = 0
        self.powerUps   = 2
    }
    
    func gameOver() {
        self.isRunning = false
        
        /*
         // SHOW LOSE SCREEN (FROM ZombieConga => SEE LOSE SCREEN on it!!)
         let loseScene = LoseScreen(size: self.size)
         //let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
         self.view?.presentScene(loseScene)
         //self.view?.presentScene(loseScene, transition:transitionEffect)
         */
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        print(mousePosition)
        
        // ------------------------------------
        // MARK: SWAP THE LEFT & RIGHT POSITION OF THE CAT
        //  If person taps left side, then move cat left
        //  If person taps right side, move cat right
        // -------------------------------------
        
        // 1. detect where person clicked
        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            self.moveCatToLeft()
        }
        else {
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            self.moveCatToRight()
        }
    }
    
    func moveCatToLeft() {
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        // change the cat's direction
        let facingRight = SKAction.scaleX(to: 1, duration: 0)
        self.cat.run(facingRight)
        // save cat's position
        self.catPosition = "left"
        //Call the cat's punch animation
        self.punchCatAnimation()

    }
    
    func moveCatToRight() {
        cat.position = CGPoint(x:self.size.width*0.75, y:100)
        // change the cat's direction
        let facingLeft = SKAction.scaleX(to: -1, duration: 0)
        self.cat.run(facingLeft)
        // save cat's position
        self.catPosition = "right"
        //Call the cat's punch animation
        self.punchCatAnimation()
    }
    
    func punchCatAnimation () {
        // ------------------------------------
        // MARK: ANIMATION OF PUNCHING CAT
        // -------------------------------------
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        let punchTextures = [image1, image2, image3, image1]
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        self.cat.run(punchAnimation)
        
        // ------------------------------------
        // MARK: WIN AND LOSE CONDITIONS
        // -------------------------------------
        // 1. if CAT and STICK are on same side - OKAY, keep going
        // 2. if CAT and STICK are on opposite sides -- YOU LOSE
        let firstChopstick = self.chopstickPositions[0]
        if (catPosition == "left" && firstChopstick == "left") {
            self.numPoints -= 1
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
            
        }
        else if (catPosition == "right" && firstChopstick == "right") {
            self.numPoints -= 1
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "left" && firstChopstick == "right") {
            self.numPoints += 1
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "left") {
            self.numPoints += 1
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        
        // ------------------------------------
        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
        //  When person taps mouse,
        //  remove a piece from the tower & redraw the tower
        // -------------------------------------
        let pieceToRemove = self.sushiTower.first
        let stickToRemove = self.chopstickGraphicsArray.first
        
        if (pieceToRemove != nil && stickToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // STICK: hide it from screen & remove from game logic
            stickToRemove!.removeFromParent()
            self.chopstickGraphicsArray.remove(at:0)
            
            // STICK: Update stick positions array:
            self.chopstickPositions.remove(at:0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // STICK: loop through the remaining sticks and redraw
            for stick in chopstickGraphicsArray {
                stick.position.y = stick.position.y - SUSHI_PIECE_GAP
            }
        }
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
}
