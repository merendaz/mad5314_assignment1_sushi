# SUSHI WATCH #

Assignment 1 of MAD5314-APPLICATION FOR WEARABLE DEVICES.

### Introduction ###

* The purpose of this activity is to explore communication between the Phone and Watch, using Sushi Tower code from MAD5244 as a starter.
* The OS used is iOS + Watch OS

### Description/Requirements ###

* #### R1:  WATCH: Use the watch to control the cat ####
	- Cat move left and right when you tap the WATCH

* #### R2:  PHONE & WATCH:  Add a game timer ###
	- Change the game so that the player gets exactly 25 seconds to hit as many sushi as they can.
	- PHONE:  Add a timer so you can see how much time is remaining.
	- The WATCH should display a warning message when at the following times:
		- 15 seconds
		- 10 seconds remaining
		- 5 seconds remaining
		- GAME OVER!

* #### R3: PHONE & WATCH:   A “more time” powerup! ###
	- At randomly intervals during the game, the phone can send the watch a “more time” powerup.
	- If the phone sends a powerup, show the powerup on the WATCH. The player has 2 seconds to accept the powerup
	- If the powerup is accepted, then add +10 seconds to the game timer.
	- Maximum 2 powerups per game session.
	- User accepts powerup by clicking on the WATCH

* #### R4: WATCH: Pause the game ###
	- Player can pause the game by pressing on the watch -- use a gesture to pause .

* #### R5: CLOUD/PHONE/WATCH: Cloud-Based High Score Board ###
	- Implement a retro high-score board, like this: [Click link here](https://www.google.com/search?q=enter+high+score+arcade&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjNjPOPo63lAhVIn-AKHU9kDeIQ_AUIEigB&biw=1280&bih=617#imgrc=_)
#### Specifically: ####
* WATCH:  
	- When user finishes a game, ask them to ENTER their name (max 3 characters)
	- Save name and high score to CLOUD  (I recommend using Parse or Firebase)
		- If you are unable to make the watch connect directly to the cloud, then you can:
			- Send high score data to the phone
			- Tell phone to send high score to the cloud

* PHONE:
	- User can view high scores on the PHONE
	- High scores should be pulled from the CLOUD


### AUTHOR ###

* Antonio Merendaz do Carmo Neto
* C0741427